/* eslint-disable no-use-before-define */
import React, { useState, useEffect } from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import { ThemeProvider } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Popover from '@material-ui/core/Popover';

// Import styles
import {useStyles} from '../styles/FilterDrawer.styles.js';

// Import Router
import {
  Route,
  Link,
  useParams,
  useLocation,
  Push
} from "react-router-dom";

export default function FilterDrawer(props) {

  // State objects and functions that manage the 'applicable' subsequent dropdown lists
  const [locations, setApplicableLocations] = useState([]);
  const [businessUnits, setApplicableBusinessUnits] = useState([]);
  const [serviceAreas, setApplicableServiceAreas] = useState([]);
  const [operatingUnits, setApplicableOperatingUnits] = useState([]);
  
  const hierarchy = props.hierarchy;
  const padding = 5;
  const [allLocations, setAllLocations] = useState([]);

  // State objects and functions that manage the current selected filter options
  const [selectedLocations, setSelectedLocations] =  useState([]);
  const [selectedBusinessUnits, setSelectedBusinessUnits] = useState([]);
  const [selectedServiceAreas, setSelectedServiceAreas] = useState([]);
  const [selectedOperatingUnits, setSelectedOperatingUnits] = useState([]);
  const [skillFilters, setSkillFilters] = useState([]);

  // API state objects that will get passed to the API (uses 'all' if no values are selected)
  const [APILocations, setAPILocations] = useState(['all']);
  const [APIBusinessUnits, setAPIBusinessUnits] = useState(['all']);
  const [APIServiceAreas, setAPIServiceAreas] = useState(['all']);
  const [APIOperatingUnits, setAPIOperatingUnits] = useState(['all']);
  const [APISkillFilters, setAPISkillFilters] = useState(['No skill filters']);

  // Flag to indicate whether URL params have been extracted yet or not, and URL param query string value 
  const [URLParamsExtracted, setURLParamsExtracted] = useState(false);
  const [URLString, setURLString] = useState(false);

  const [anchorEl, setAnchorEl] = React.useState(null);
  const popOverOpen = Boolean(anchorEl);
  const popOverID = popOverOpen ? 'simple-popover' : undefined;

  // This is used to loop through the different parameters and fetch the appropriate state update method or value. It is used in updating the URL parameters or getting the right values from the URL parameters
  const paramObject = {'locations':[setSelectedLocations, setAPILocations, selectedLocations], 'businessUnits': [setSelectedBusinessUnits, setAPIBusinessUnits, selectedBusinessUnits], 'serviceAreas':[setSelectedServiceAreas, setAPIServiceAreas, selectedServiceAreas], 'operatingUnits': [setSelectedOperatingUnits, setAPIOperatingUnits, selectedOperatingUnits], 'skillFilters': [setSkillFilters, setAPISkillFilters, skillFilters]};
  
  //Styles object
  const classes = useStyles();

  // Actual value of URL params
  const urlParams = useLocation().search

  // Maintain the current set of selections in a string, so that on click of the generate board button, this string can be used to append to the URL parameters
  useEffect(() => {
    updateURLString();
  }, [selectedLocations, selectedBusinessUnits, selectedServiceAreas, selectedOperatingUnits, skillFilters])

  // check if there are params in the URL route already, and if yes - set the appropriate selection values and generate board
  useEffect(() => {
    if(props.hierarchyLoaded){
      if(urlParams){
        extractURLParams();
      }
    }
  }, [props.hierarchyLoaded])

  useEffect(() => {
    if(URLParamsExtracted){
      generateBoard();
    }
  }, [URLParamsExtracted])
  
  useEffect(() => {
    if(Object.keys(props.hierarchy).length){
      setAllLocations(props.hierarchy.allValues.allValues.allLocations);
      setApplicableLocations(props.hierarchy.allValues.allValues.allLocations);
      setApplicableBusinessUnits(props.hierarchy.allValues.allValues.allBusinessUnits);
      setApplicableServiceAreas(props.hierarchy.allValues.allValues.allServiceAreas);
      setApplicableOperatingUnits(props.hierarchy.allValues.allValues.allOperatingUnits);
    }
  }, [props.hierarchyLoaded])

  // Location selection changes
  useEffect(() => {
    locationSelectionChanges(selectedLocations);
  }, [selectedLocations]);

  // Business Unit selection changes
  useEffect(() => {
    businessUnitSelectionChanges();
  }, [selectedBusinessUnits])

  // Service Area selection changes
  useEffect(() => {
    serviceAreaSelectionChanges();
  }, [selectedServiceAreas])

  // Operating Unit selection changes
  useEffect(() => {
    operatingUnitSelectionChanges();
  }, [selectedOperatingUnits]);

  // Skill Filter Added
  useEffect(() => {
    skillFilterAdded();
    }, [skillFilters]);

  const generateBoard = () => {
    // Use API filter state objects to ensure no empty arrays are passed to API
    props.getEmployees(APILocations, APIBusinessUnits, APIServiceAreas, APIOperatingUnits, APISkillFilters);
  }

  const extractURLParams = () => {
    // Perform some manipulations on the URL params including replacing '%20' and splitting by the Param delimiter ('&&' as '&' is used in some team names)
    const splitParams = urlParams.replace('?','').replace(/%20/g,' ').split('&&');

    // For each of the parameters found in the URL, get its appropriate 'name' (i.e. 'locations') and the value of the parameter (i.e. 'Brisbane')
    splitParams.map((filter) => {
      const filterName = filter.split('=')[0];
      const filterValue = filter.split('=')[1];

      // Search in the paramObject to find the right function that will allow us to update the current selected value for that parameter by what has been fetched from the URL
      // Update the selected value and API value for that parameter
      paramObject[filterName][0](filterValue.split(','));
      paramObject[filterName][1](filterValue.split(','));

    })

    // Update the extraction flag to indicate that generateBoard is ready to be triggered. This flag helps prevent sync / async issues when generateBoard is just called directly from here
    setURLParamsExtracted(true);
  }

  // This function always maintains the current string that will be appended to the end of the URL based on what the user has selected in the dropdowns
  const updateURLString = () => {
    var paramString = '?';

    // Loop through the paramObject constant, and for each param type in there (keys), check if there is a selection made in this dropdown
    Object.keys(paramObject).map(param => {

      // If there is a selection, get the value of that selection and append it to the paramString
      if(paramObject[param][2].length){
        paramString = paramString + param + '=' + paramObject[param][2] + '&&'
      }
    })

    // Slice the trailing '&&' from the end of the paramString before setting its value
    setURLString(paramString.slice(0, paramString.length - 2));
  }

  const locationSelectionChanges = () => {
    let nonEmptyLocations = [];
    let applicableBUs = [];
    let applicableSAs = [];
    let applicableOUs = [];

    // if no Locations are selected, set the 'nonEmptyLocations' value to all of the Locations
    if(selectedLocations.length){
      nonEmptyLocations = selectedLocations;
      setAPILocations([nonEmptyLocations]);
    }
    else{
      nonEmptyLocations = allLocations;
      setAPILocations('all');
    }

    setApplicableLocations(nonEmptyLocations);

    // for each Location in nonEmptyLocations
    for(var i = 0; i < nonEmptyLocations.length; i++){

      // set to variable for readability - stores Location currently being examined
      var loc = nonEmptyLocations[i]
      
      // get the array of business units associated with this Location
      var BU = Object.keys(hierarchy[loc]);
  
      for(var j = 0; j < BU.length; j++){

        // push the Business Unit value onto the applicableBUs array, as this will be used to populate the dropdown
        applicableBUs.push(BU[j]);

        // get the array of Service Areas associated with this business unit and Location combination
        var SA = Object.keys(hierarchy[loc][BU[j]]);
        
        for(var k = 0; k < SA.length; k++){

          // push the value on the applciableSAs array, as this will be used to populate the dropdown
          applicableSAs.push(SA[k])

          // get the array of Operating Units associated with this business unit, Location and Service Area combination
          var OU = hierarchy[loc][BU[j]][SA[k]];

          for(var l = 0; l < OU.length; l++){

            // push the value onto the applicableOUs array, as this will be used to populate the dropdown
            applicableOUs.push(OU[l])
          }
        }
      }
    }
     
    // create unique lists to set dropdown values to
    let uniqueBUs = Array.from(new Set(applicableBUs));
    let uniqueSAs = Array.from(new Set(applicableSAs));
    let uniqueOUs = Array.from(new Set(applicableOUs));

    setApplicableBusinessUnits(uniqueBUs);
    setApplicableServiceAreas(uniqueSAs);
    setApplicableOperatingUnits(uniqueOUs);
  }

  const businessUnitSelectionChanges = () => {
    let nonEmptyBUs = [];
    let applicableSAs = [];
    let applicableOUs = [];

    // if there are no Business Units selected, set the 'nonEmptyBUs' value to the current dropdown BU values
    if(selectedBusinessUnits.length){
      nonEmptyBUs = selectedBusinessUnits;
      setAPIBusinessUnits(selectedBusinessUnits);
    }
    else{
      nonEmptyBUs = businessUnits;
      setAPIBusinessUnits('all');
    }

    // for each Location in selectedLocations
    for(var i = 0; i < locations.length; i++){

      // set to variable for readability - stores Location currently being examined
      var loc = locations[i]
  
      // get the array of Business Units associated with this Location. It is necessary to first loop through all Business Units, then check if they sit within the selected value, due to some Locations not having all Business Units
      var BU = Object.keys(hierarchy[loc]);
  
      for(var j = 0; j < BU.length; j++){

        // check if the Business Unit is one of the values selected
        if(nonEmptyBUs.includes(BU[j])){
          
          // get the array of Service Areas associated with this Business Unit and Location combination
          var SA = Object.keys(hierarchy[loc][BU[j]]);
        
          for(var k = 0; k < SA.length; k++){
            applicableSAs.push(SA[k])
  
            // get the array of Operating Units associated with this Business Unit, Location and Service Area combination
            var OU = hierarchy[loc][BU[j]][SA[k]];
            
            for(var l = 0; l < OU.length; l++){
              applicableOUs.push(OU[l])
            }
          }
        }

      }
    }
     
    // create unique lists to set dropdown values to
    let uniqueSAs = Array.from(new Set(applicableSAs));
    let uniqueOUs = Array.from(new Set(applicableOUs));

    setApplicableServiceAreas(uniqueSAs);
    setApplicableOperatingUnits(uniqueOUs);
  }

  const serviceAreaSelectionChanges = () => {
    let nonEmptySAs = [];
    let nonEmptyBUs = [];
    let applicableOUs = [];

    // if no Service Areas are selected, set the 'nonEmptySAs' value to the current SA dropdown options
    if(selectedServiceAreas.length){
      nonEmptySAs = selectedServiceAreas;
      setAPIServiceAreas(selectedServiceAreas);
    }
    else{
      nonEmptySAs = serviceAreas;
      setAPIServiceAreas('all');
    }

     // if no BusinessUnits are selected, set the 'nonEmptyBUs' value to the current BU dropdown options
     if(selectedBusinessUnits.length){
      nonEmptyBUs = selectedBusinessUnits;
    }
    else{
      nonEmptyBUs = businessUnits;
    }

    // for each Location in selectedLocations
    for(var i = 0; i < locations.length; i++){

      // set to variable for readability - stores Location currently being examined
      var loc = locations[i]
        
        var BU = Object.keys(hierarchy[loc]);
  
        // loop through all the valid Business Units for this Location
        for(var j = 0; j < BU.length; j++){

          // but only proceed if the current Business Unit is within the selectedBusinessUnits array
          if(nonEmptyBUs.includes(BU[j])){
            
            var SA = Object.keys(hierarchy[loc][BU[j]]);

            // loop through all of the Service Areas associated with the Location / Business Unit combination
            for(var k = 0; k < SA.length; k++){

              // only proceed if the current Service Area is within the array of selected Service Areas
              if(nonEmptySAs.includes(SA[k])){
                
                // get the array of Operating Units associated with this Businesss Unit, Location and Service Area combination
                var OU = hierarchy[loc][BU[j]][SA[k]];
                
                for(var l = 0; l < OU.length; l++){
                  applicableOUs.push(OU[l])
                }
              }
    
            }
          }

      }
    }
     
    //create unique list of values for dropdowns
    let uniqueOUs = Array.from(new Set(applicableOUs));

    // setSelectedServiceAreas(nonEmptySAs);
    setApplicableOperatingUnits(uniqueOUs);
  }

  const operatingUnitSelectionChanges = () => {
    var nonEmptyOUs = [];

    // if no Operating Units are selected, set the 'nonEmptyOUs' value to the current OU dropdown options
    if(selectedOperatingUnits.length){
      nonEmptyOUs = selectedOperatingUnits;
      setAPIOperatingUnits(selectedOperatingUnits);
    }
    else{
      nonEmptyOUs = operatingUnits;
      setAPIOperatingUnits('all');
    }
  }

  const skillFilterAdded = () => {
    if(skillFilters.length){
      setAPISkillFilters(skillFilters);
    }
    else{
      setAPISkillFilters('No skill filters')
    }
  }

  const displayResultCount = () => {
    if(props.resultCount >= 0){
      return(
        <Typography>
          Number of results: {props.resultCount}
        </Typography>
      )
    }

  }

  const copyLinkClick = (event) => {
    setAnchorEl(event.currentTarget);

    //code to copy url link to the filtered photoboard for bookmarking
    var dummyInput = document.createElement('input'),

    //copy the page url + router query
    text = document.location.origin + URLString;

    document.body.appendChild(dummyInput);
    dummyInput.value = text;
    dummyInput.select();
    document.execCommand('copy');
    document.body.removeChild(dummyInput);
  }
  
  const handlePopOverClose = () => {
    setAnchorEl(null);
  };

  return (
    <ThemeProvider theme = {props.theme}>
      <Route>
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="left"
          open={props.open}
          classes={{
          paper: classes.drawerPaper,
          }}
        >
          <div className={classes.drawerHeader}>
            <IconButton onClick={props.handleDrawerClose}>
              <ChevronLeftIcon /> 
            </IconButton>
          </div>

          <Box p={3}>
            <Typography variant = 'h5' >
              Select Filters
            </Typography>
            <Box p={1}>
            </Box>
            <Typography variant = 'body'>
              Select filters from each the below dropdowns, then click 'Generate Board' to view the digital photoboard.
            </Typography>
            <Box p={3}>
              <Divider/>
            </Box>
  
            {/* Locations Dropdown */}
            <Autocomplete
            multiple
            autoHighlight
            value = {selectedLocations}
            id="Locations"
            onChange={(changedSelection,selectedLocs) => setSelectedLocations(selectedLocs)}
            options={allLocations}
            getOptionLabel={(option) => option}
            style={{ padding: padding }}
            renderTags={(value, getTagProps) =>
              value.map((option, index) => (
                <Chip variant="default" color = 'primary' label={option} {...getTagProps({ index })} />
              ))
            }
            renderInput={(params) => (
                <TextField
                {...params}
                variant="outlined"
                label="Locations"
                />
            )}
            />

            {/* Business Units Dropdown */}
            <Autocomplete
            multiple
            value = {selectedBusinessUnits}
            autoHighlight
            id="businessUnits"
            options={businessUnits}
            onChange={(changedSelection, selectedBUs) => setSelectedBusinessUnits(selectedBUs)}
            getOptionLabel={(option) => option}
            style={{ padding: padding }}
            renderTags={(value, getTagProps) =>
              value.map((option, index) => (
                <Chip variant="default" color = 'primary' label={option} {...getTagProps({ index })} />
              ))
            }
            renderInput={(params) => (
              <TextField
                {...params}
                variant="outlined"
                label="Business Units"
              />
            )}
            />

            {/* Service Areas Dropdown */}
            <Autocomplete
              multiple
              value = {selectedServiceAreas}
              autoHighlight
              id="serviceAreas"
              options={serviceAreas}
              onChange={(changedSelection, selectedSAs) => setSelectedServiceAreas(selectedSAs)}
              getOptionLabel={(option) => option}
              style={{ padding: padding }}
              renderTags={(value, getTagProps) =>
                value.map((option, index) => (
                  <Chip variant="default" color = 'primary' label={option} {...getTagProps({ index })} />
                ))
              }
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant="outlined"
                  label="Service Areas"
                />
              )}
            />
        
            {/* Operating Units Dropdown */}
            <Autocomplete
              multiple
              autoHighlight
              value = {selectedOperatingUnits}
              id="operatingUnits"
              options={operatingUnits}
              getOptionLabel={(option) => option}
              onChange={(changedSelection, selectedOUs) => setSelectedOperatingUnits(selectedOUs)}
              color = 'primary'
              style={{ padding: padding }}
              renderTags={(value, getTagProps) =>
                value.map((option, index) => (
                  <Chip variant="default" color = 'primary' label={option} {...getTagProps({ index })} />
                ))
              }
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant="outlined"
                  label="Operating Units"
                  color = 'primary'
                />
              )}
            />

            {/* Skill Filter entry form */}
            <Autocomplete
              multiple
              id="skills"
              options={[]}
              value = {skillFilters}
              freeSolo
              onChange={(changedSelection, skillsAdded) => setSkillFilters(skillsAdded)}
              style={{ padding: padding }}
              variant = 'outlined'
              renderTags={(value, getTagProps) =>
                value.map((option, index) => (
                  <Chip variant="default" color = 'primary' label={option} {...getTagProps({ index })} />
                ))
              }
              renderInput={(params) => (
                <TextField {...params} variant="outlined" label="Skills" placeholder="Type skills to filter by" />
              )}
            />

            <Box p={3}>
              <Divider/>
            </Box>
        
            <Box p={1} display='inline' >
              <Button variant="outlined" 
                color="default"
                to = {URLString}
                component = {Link}
                onClick={() => { generateBoard() }}
              >
                GENERATE
              </Button>
            </Box>
        
            <Box p={1} display='inline'>
              <Button variant="outlined" 
                color="default"
                onClick={(event) => { copyLinkClick(event) }}
                > 
                COPY LINK
              </Button>
              <Popover
                id = {popOverID}
                open = {popOverOpen}
                anchorEl = {anchorEl}
                onClose = {handlePopOverClose}
                anchorOrigin = {{
                  vertical: 'bottom',
                  horizontal: 'center',
                }}
                transformOrigin = {{
                  vertical: 'top',
                  horizontal: 'center',
                }}
              >
                <Typography variant = 'body1'>A link to this exact filtered board has been copied to your clipboard</Typography>
              </Popover>
            </Box>
        
            <Box p={3}>
              <Divider/>
            </Box>
        
            {displayResultCount()}

          </Box>
        </Drawer>
      </Route>
    </ThemeProvider>
    
  )
  
};
