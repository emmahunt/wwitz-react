/* eslint-disable no-use-before-define */
import Link from '@material-ui/core/Link';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { withStyles } from '@material-ui/core/styles';
import React from 'react';
import Typography from '@material-ui/core/Typography';

// Import styles
import styles from '../styles/RoleList.styles.js';

class RoleList extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            activeRole: null,
        }
    }

    componentDidMount() {
        document.addEventListener('scroll', () => {
          this.scrollSpy();
        });

    }
    
    scrollSpy() {
        this.props.roles
            .map(role => {
                var element = document.getElementById(role);
                if (element) {
                    if (this.isInView(element)){
                        this.setState({activeRole: element.id})
                    }
                }
                
                // Placeholder return to suppress warning message. What should this return?
                return '';
            })
    }

    isInView = (element: HTMLElement) => {
        const rect = element.getBoundingClientRect();
        return rect.bottom <= window.innerHeight;
    }

    scrollTo(role){
        var element = document.getElementById(role.role);
        element.scrollIntoView({behavior: "smooth", block: "start", inline: "start"});
    }

    render() {
        const { classes } = this.props;

        if (this.props.compactView) return(null)
        else{
        return (
            <List dense={true} className = {classes.roleList}>
                {(this.props.roles).map((role) => 
                    <ListItem className = { this.state.activeRole === role ? classes.activeRoleListItem : classes.roleListItem } key = {role}>
                        <Link onClick = {(click) => this.scrollTo({role})}>
                            <Typography className = { this.state.activeRole === role ? classes.activeRoleListText : classes.roleListText }>
                                {role}
                            </Typography>
                        </Link>
                    </ListItem>
                )}
            </List>   
        );
                }
    }
}

export default withStyles(styles.styles)(RoleList);