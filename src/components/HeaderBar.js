/* eslint-disable no-use-before-define */
import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import IconButton from '@material-ui/core/IconButton';
import { ThemeProvider } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import MenuIcon from '@material-ui/icons/Menu';
import clsx from 'clsx';
import React from 'react';

// Import styles
import { useStyles } from '../styles/HeaderBar.styles.js';

export default function HeaderBar(props) {
    
    const classes = useStyles();

    const checkboxChanged = () => {
        const checkboxValue = props.compactMode
        if(checkboxValue){
            props.setCompactMode(false)
        }
        else{
            props.setCompactMode(true)
        }
    }

    return (
    <ThemeProvider theme = {props.theme}>
        <AppBar 
            className = {classes.titleBar}
        >
            <Toolbar className = {classes.toolBar}>
                <Typography variant="h4" font = 'Roboto'>
                    Who's Who in the Zoo
                </Typography>
                <Typography variant="h5" font = 'Roboto'>
                    Deloitte Australia - digital photoboard
                </Typography>
            </Toolbar>
        </AppBar>

        <AppBar
            position="fixed"
            // Class name is 'appBar' unless open is set to true - in which case class name is 'appBarShift' 
            className={clsx(classes.appBar, {
                [classes.appBarShift]: props.open,
            })}
        >
            <Toolbar>
                <IconButton
                    // color="inherit"
                    aria-label = "open drawer"
                    onClick = {props.handleDrawerOpen}
                    edge = "start"
                    // Class name is 'menuButton' unless open is set to true - in which case class name is 'hide' 
                    className = {clsx(classes.menuButton, props.open && classes.hide)}
                >
                    <MenuIcon />
                </IconButton>

                <Box width = '50%'>
                    <Typography variant = "body1" 
                    className = {clsx(classes.menuButton, props.open && classes.hide)}noWrap>
                        Open Filters
                    </Typography>
                </Box>

                <Box width = '50%' justifyContent = 'flex-end' display = 'flex'>
                    <FormControlLabel
                        control={<Checkbox 
                                    name="compactMode" 
                                    color="primary"
                                    checked={props.compactMode}
                                    onChange={checkboxChanged}/>}
                        label="Compact Print View"
                    />
                </Box>

            </Toolbar>

        </AppBar>

        </ThemeProvider>
    )
}