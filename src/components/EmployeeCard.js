/* eslint-disable no-use-before-define */
import React from 'react';
import Card from '@material-ui/core/Card';  
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useStyles } from '@material-ui/core/styles';
import CardMedia from '@material-ui/core/CardMedia';
import { Divider } from '@material-ui/core';
import Link from '@material-ui/core/Link';

import styles from '../styles/EmployeeCard.styles.js'

import FALLBACK_IMAGE from '../fallBackImage.PNG';

const onMediaFallback = event => event.target.src = FALLBACK_IMAGE;
const dpnProfileURL = 'https://people.deloitteresources.com/dpn/index.html#/Profile/';

const cardHeaderBannerColor = styles.cardHeaderBannerColor;

export default function EmployeeCard(props) {

    // 'classes' is available as a prop due to the export withStyes(style) statement at the bottom of this file
    // const { classes } = ((props.compactMode) ? props.compact : props.large);

    const useStyles = makeStyles(((props.compactMode) ? styles.style.compact : styles.style.large));

    const classes = useStyles();


    return (
        <Card className = {classes.card}>

            {/* Person Business Unit */}
            <CardHeader
                style={cardHeaderBannerColor[props.emp.service_Area_Style]}
                className = {classes.cardHeader}
                subheader = {
                    <Typography className={classes.cardHeaderText} variant={'caption'}>
                        {props.emp.business_Unit}
                    </Typography>
                }
            />
            
            {/* Upper contents of the card  */}
            <CardContent className = {classes.cardContentUpper}>
            
                {/* Person Location */}
                <Typography className = {classes.upperCardContentFontSizeAndHeight}
                    variant = 'overline'
                    color = 'textSecondary'
                    align = 'right'
                >
                    <div>{props.emp.location}</div>
                </Typography>

                {/* Person Service Area */}
                <Typography className = {classes.upperCardContentFontSizeAndHeight}
                    variant = 'caption'
                    color = 'textSecondary'
                    align = 'right'
                >
                    <div>{props.emp.service_Area}</div>
                </Typography>

                {/* Person Location */}
                <Typography className = {classes.upperCardContentFontSizeAndHeight}
                    variant = 'overline'
                    color = 'textSecondary'
                    align = 'right'
                >
                    <div>{props.emp.mobile_Num}</div>
                </Typography>

            </CardContent>

            <Divider variant = "middle" />

            {/* Lower contents of the card */}
            <CardContent className = {classes.cardContentLower}>

                {/* Person Full name and DPN link */}
                <Typography className = {classes.fullnameFontSizeAndHeight}
                    variant = 'subtitle1'
                    color = 'textPrimary'
                >
                    <Link 
                        href = {dpnProfileURL + [props.emp.username]}
                        target = '_blank'
                        color = 'inherit'
                        variant = 'inherit'
                    >
                        {props.emp.full_Name}
                    </Link>
                </Typography>
            </CardContent>

            
            {/* Person DPN profile picture */}
            <CardMedia className = {classes.media}
                component = "img"
                image = {'https://people.deloitteresources.com/User%20Photos/Profile%20Pictures/i_0%C7%B5.t_adfs%20prod_'+props.emp.username+'_MThumb.jpg'}
                onError = {onMediaFallback}
            />
            <CardContent className = {classes.cardHoverOver} id = 'cardHoverOver'>

                {/* Card Hover Over */}
                <Typography className = {classes.qualification}
                    color = 'textPrimary'
                >
                    {props.emp.qualification}
                </Typography>
                <Typography className = {classes.bio}
                    color = 'textPrimary'
                >
                    {props.emp.bio}
                </Typography>
            </CardContent>
            
        </Card>  
    )
    }
