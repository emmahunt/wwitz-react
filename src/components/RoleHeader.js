/* eslint-disable no-use-before-define */
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import React from 'react';

// Import styles
import {useStyles} from '../styles/RoleHeader.styles.js';

export default function RoleHeader(props) {
    const styles = useStyles();

    // If the compact mode is selected, display the header differently (inline) to save space
    if(props.compactMode){
        return(
            <div>
                <Grid item xs = {12}>
                    <Box width = '140px' m = {'7px'}>
                        <Typography className = {styles.sidewaysRole}>
                            {props.role}
                        </Typography>
                    </Box>
                </Grid>
            </div>
        )
    }

    // If not in compact mode, display the header as normal
    else{
        return (
            <Box minWidth = '100%'>
                <Typography variant = 'h3' >
                    {props.role}
                </Typography>

                {/* This is the hidden anchor link that enables scrolling to the given role */}
                <div className = {styles.anchorLink} id = {props.role}></div> 
            </Box>
    
        )
    }

}