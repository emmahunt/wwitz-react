
import { makeStyles } from '@material-ui/core/styles';

const drawerWidth= '16%';
const titleBarHeight = '100px';

const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
    },
    
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      marginLeft: `calc(0% - ${drawerWidth})`,
      top:titleBarHeight,
      position: 'relative'
    },
    contentShift: {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
      top:titleBarHeight,
      position: 'relative'

    },
    drawerHeader: {
      display: 'flex',
      alignItems: 'center',
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
      justifyContent: 'flex-end',
    },
    loadingModal: {
      top: `50%`,
      left: `50%`,
      transform: `translate(0%, ` + titleBarHeight + `)`,
      border: '0px',
      
    },
    loadingModalPaper: {
      position: 'absolute',
      width: '300px',
      height: '30%',
      justifyContent: 'center',
      top: `20%`,
      left: `40%`,
      backgroundColor: theme.palette.background.paper,
      border: '0px',
      padding: theme.spacing(2, 4, 3),
      outline: 'none'
      
    },
    loadingIcon: {
      position: 'fixed',
      display: 'block',
      padding: theme.spacing(5)
    },
    noResultsImage: {
      height: '60%',
      
    },
    gridContainer: {
      width: 'auto'
      
    },
    roleBoxCompact: {
      display: "contents",
      flexDirection: "row",
      flexWrap: "wrap",
      justifyContent: "flex-start",
      alignItems: "flex-start"
    },
    roleBox: {
    }

}))

export {useStyles}