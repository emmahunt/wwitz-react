const styles = {

    roleList: {
        position: 'fixed',
        cursor: 'pointer',
    },
    activeRoleListItem: {
        borderLeft: '4',
        borderColor: 'green', 
        borderLeftStyle: 'solid',
    },
    roleListText: {
        color: 'gray',
        fontSize: '12px',
    },
    activeRoleListText: {
        fontSize: '13px',
    }
  };

export default {styles: styles}