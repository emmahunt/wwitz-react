const topBorderHeight = 18;
const headerLocationHeight = 30;
const headerNameHeight = 24;

const cardPictureHeight = 200;
const cardTotalHeight = topBorderHeight + headerLocationHeight + headerNameHeight + cardPictureHeight;

const style = {
    large: {
        card: {
            width: 210,
            maxWidth: 210,
            margin: 5,
            display: 'inline-block',
            boxShadow: 2,
            borderRadius: 5,
            maxHeight: cardTotalHeight,
            '&:hover': {
                boxShadow: '0 16px 70px -12.125px rgba(0,0,0,0.4)',
            },
            '&:hover #cardHoverOver': {
                display: 'block',
            }
        },
        cardHeader: {
            height: topBorderHeight,
            marginLeft: 0,
            padding: 0,
        },
        cardHeaderText: {
            marginLeft: 5,
            color: 'white',
            variant: 'caption',
        },
        cardContentUpper: {
            padding: 0,
            marginLeft: 5,
            marginRight: 5,
            minHeight: '45px',
        },
        cardContentLower: {
            padding: 0,
            marginLeft: 5,
            marginRight: 5,
        },
        media: {
            width: '100%',
            height: cardPictureHeight,
        },
        fullnameFontSizeAndHeight: {
            lineHeight: 1.8,
            fontSize: '0.85rem',
        },
    upperCardContentFontSizeAndHeight: {
        lineHeight: 1.5,
        fontSize: '0.65rem',
        },
        cardHoverOver: {
            top: -cardPictureHeight,
            height: cardPictureHeight,
            maxHeight: cardPictureHeight,
            position: 'relative',
            color: 'red',
            padding: 5,
            backgroundColor: 'white',
            display: 'none',
            overflowY: 'scroll',
            
        },
        qualification: {
            lineHeight: 1.8,
            fontSize: '0.8rem',
            fontStyle: 'italic',
            padding: 5,

        },
        bio: {
            lineHeight: 1.8,
            fontSize: '0.6rem',
            padding: 5,

        },
    },
    compact: {
        card: {
            width: 210/1.5,
            maxWidth: 210/1.5,
            margin: 7,
            boxShadow: 2,
            borderRadius: 5,
            maxHeight: cardTotalHeight/1.5,
            '&:hover': {
                boxShadow: '0 16px 70px -12.125px rgba(0,0,0,0.4)',
            },
            '&:hover #cardHoverOver': {
                display: 'block',
            }
        },
        cardHeader: {
            height: topBorderHeight/2,
            marginLeft: 0,
            padding: 0,
        },
        cardHeaderText: {
            display: 'none',
        },
        cardContentUpper: {
            display: 'none',
        },
        cardContentLower: {
            padding: 0,
            marginLeft: 5,
            marginRight: 5,
        },
        media: {
            width: '100%',
            height: cardPictureHeight/1.3,
        },
        fullnameFontSizeAndHeight: {
            lineHeight: 1.8,
            fontSize: '0.8rem',
        },
    upperCardContentFontSizeAndHeight: {
        lineHeight: 1.5,
        fontSize: '0.4rem',
        },
        cardHoverOver: {
            display: 'none'
        },
        qualification: {
            display: 'none'
        },
        bio: {
            display: 'none'
        },
    }
  };
  
  // Background color of the cardHeader banner.
  const cardHeaderBannerColor = {
      
    Style1: {
        backgroundColor: '#C4D600'
    },
    Style2: { 
        backgroundColor: '#43B02A'
    },
    Style3: {
    backgroundColor: '#046A38'
    },
    Style4: {
        backgroundColor: '#6FC2B4'
    },
    Style5: {
        backgroundColor: '#0097A9'
    },
    Style6: {
        backgroundColor: '#53565A'
    },
    Style7: {
        backgroundColor: '#00A3E0'
    },
    Style8: {
        backgroundColor: '#0076A8'
    },
    Style9: {
        backgroundColor: '#012169'
    },
    Style10: {
        backgroundColor: '#9DD4CF'
    },
    Style11: {
        backgroundColor: '#97999B'
    },
    Style12: {
        backgroundColor: '#62B5E5'
    },
    Style13: {
        backgroundColor: '#004F59'
    },
    Style14: {
        backgroundColor: '#000000'
    }
  };

export default {cardHeaderBannerColor: cardHeaderBannerColor, style: style}