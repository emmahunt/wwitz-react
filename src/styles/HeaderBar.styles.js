
import { makeStyles } from '@material-ui/core/styles';

const drawerWidth= '16%';
const appBarHeight= '40px';
const titleBarHeight = '100px';

const useStyles = makeStyles((theme) => ({
    titleBar:{
      height: titleBarHeight,
      background: theme.palette.common.black,
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex'
    },
    toolBar:{
      display: 'block'
    },
    appBar: {
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      top: titleBarHeight,
      height: appBarHeight,
      background: theme.palette.grey.cool7,
      justifyContent: 'center',
      display: 'flex'
    },
    appBarShift: {
      width: `calc(100% - ${drawerWidth})`,
      marginLeft: drawerWidth,
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      margin: theme.spacing(1),
      color: theme.palette.main
    },
    hide: {
      display: 'none',
    }
}))

export {useStyles}