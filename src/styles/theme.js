import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: {
        // light: will be calculated from palette.primary.main,
        main: '#046A38',
        // dark: will be calculated from palette.primary.main,
        // contrastText: will be calculated to contrast with palette.primary.main
        },
        secondary: {
        main: '#0D8390',
        // dark: will be calculated from palette.secondary.main,
        },
        grey: {
            cool2: '#D0D0CE',
            cool7: '#97999B',
            cool11: '#53565A'
            // Used by `getContrastText()` to maximize the contrast between
            // the background and the text.
        },
        contrastThreshold: 3,
        // Used by the functions below to shift a color's luminance by approximately
        // two indexes within its tonal palette.
        // E.g., shift from Red 500 to Red 300 or Red 700.
        tonalOffset: 0.4,
        green:{
            main: '#86BC25',
            light: '#d1e7a3'
        },
        background:{
            secondary: '#D0D0CE'
        }
    },
    typography:{
        h4: {
            color: '#86BC25'
        }
    }
});

export default theme;