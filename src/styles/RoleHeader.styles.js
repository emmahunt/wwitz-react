
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({

    anchorLink: {
        display: 'block',
        top: '-200px',
        height: '10px',
        visibility: 'hidden',
        position: 'relative',
    },
    roleHeaderText: {
        fontSize: '25px',
    },
    sidewaysRole: {
        transform: 'rotate(180deg)',
        writingMode: 'vertical-rl',
        fontSize: '1.6rem',
        overflowWrap: 'anywhere',
        maxHeight: '180px',
        minHeight: '180px',
        minWidth: '140px',
        display: 'flex',
        alignItems: 'flex-end', 
        // justifyContent: 'center',
    }

}))

export {useStyles}