import { makeStyles } from '@material-ui/core/styles';

const drawerWidth= '16%';
const appBarHeight= '40px';
const titleBarHeight = '100px';

const useStyles = makeStyles((theme) => ({
  button: {
    padding: '10px'
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    top: titleBarHeight,
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
}))

export {useStyles}