import Box from '@material-ui/core/Box';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import Modal from '@material-ui/core/Modal';
import { ThemeProvider } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import clsx from 'clsx';
import React, { useEffect, useState } from 'react';

// Import Router
import { BrowserRouter as Router } from "react-router-dom";

// Import Components
import EmployeeCard from './components/EmployeeCard.js';
import FilterDrawer from './components/FilterDrawer.js';
import HeaderBar from './components/HeaderBar.js';
import RoleHeader from './components/RoleHeader.js';
import RoleList from './components/RoleList.js';
import noResultsImg from './images/noResults.png';

// Import Styles
import { useStyles } from './styles/App.styles.js';
import theme from './styles/theme.js';

export default function Site() {
  
  const classes = useStyles();
  const [loadingStatus, setLoadedingStatus] = useState(false);
  const [error, setError] = useState('');
  const [hierarchy, setHierarchy] = useState({});
  const [hierarchyLoaded, setHierarchyLoaded] = useState(false);
  const [employees, setEmployees] = useState({});
  const [open, setOpen] = useState(true);
  const [generateBoardValue, generateBoardClicked] = useState(0);
  const [resultCount, setResultCount] = useState(-1);

  const [compactMode, setCompactMode] = useState(false);

  const getEmployees = (selectedCities, selectedBusinessUnits, selectedServiceAreas, selectedOperatingUnits, skillFilters) => {
    
    setLoadedingStatus(true);
    // Increment the generateBoardClicked state, so that it will register as a new value and trigger the useEffect hook
    generateBoardClicked( + 1); 
    
    // Fetch the results from the API
    fetch("https://localhost:5001/api/Employee/" + selectedCities + "/" + selectedBusinessUnits + "/" + selectedServiceAreas + "/" + selectedOperatingUnits + "/" + skillFilters )
      .then(res => res.json())
      .then(
        (result) => {
          setLoadedingStatus(false);
          setEmployees(result.item1);
          setResultCount(result.item2);
        })
      // if there's an error - update the state object to display an error message
      .catch(error => {
        setLoadedingStatus(false);
        setError(error);
      }
    )
  }

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  // This is split into a function so that it can be called to regenerate the board in the useEffect hook when the user clicks 'Generate board'
  const displayEmployees = () => {
    return(
      //Loop through the roles (object keys) in the employee result set, and generate a RoleHeader component for each
      Object.keys(employees).map(function(role,i)
      {
        return (
          <Grid container xs = {12} className = {((compactMode) ? classes.roleBoxCompact: classes.roleBox)} >
            
            <RoleHeader role = {role} key = {role} compactMode = {compactMode}></RoleHeader>

         {/* Loop through the employees in the array corresponding to the current role, and generate an EmployeeCard component for each */}
          {(employees[role].map(function(person)
            {
            return (
              <Grid item>
                <EmployeeCard emp = {person} key = {person.username} compactMode = {compactMode}></EmployeeCard>
              </Grid>
            )
            }
          ))}
          </Grid >
        )
      }

    )
    )
  }

  const displayLoadingIcon = () => {
    if(loadingStatus){
      return(
      <Modal open color = 'grey.cool2' className = {classes.loadingModal}>  
        <Box className = {classes.loadingModalPaper}>
        <Typography variant = 'h5'>
          Loading ...
        </Typography>
        <CircularProgress color="secondary" size = {200} className = {classes.loadingIcon}/>
        </Box>
      </Modal>
      )
    }
  }

  const displayNoResults = () => {
    if(resultCount ==0){
      return(
        <Grid item>
        <Typography variant = 'h2'>No results</Typography>
        <img 
        src = {noResultsImg}
        className = {classes.noResultsImage}
        />
        </Grid>
      )
    }
  }

  // This function determines the structure of the site (i.e. if RoleList gets displayed or not) depending on whether or not compactMode is selected
  const handleCompactMode = () => {
    if(compactMode){
      return(
        <Grid container spacing={1}> 

          <Grid item xs={12}>
            <Grid container justify = 'flex-start' p ={3}>
            {displayEmployees()}
            {displayNoResults()}
            </Grid>
          </Grid>

        </Grid>
      )
    }

    else{
      return(
        <Grid container spacing={1}> 
            <Grid item xs={11}>
              <Grid container justify = 'flex-start' xs = {12}p ={3}>
              {displayEmployees()}
              {displayNoResults()}
              </Grid>
            </Grid>

            <Grid item xs={1}>{/* Use the RoleList component */}
              <RoleList roles = {Object.keys(employees)}/>
          </Grid>

        </Grid>
      )
    }
  }

  // This will automatically close the filter drawer if compact mode is selected
  useEffect(() => {
    displayEmployees()
  }, [generateBoardValue])

  useEffect(() => {
    if(compactMode){
      setOpen(false)
    }
  }, [compactMode])


  async function getHierarchy() {
    await fetch("https://localhost:5001/api/Hierarchy/hierarchy")
        .then(res => res.json())
        .then(
          (result) => {
            setHierarchy(result);
            setHierarchyLoaded(true);
            }
        )
        // if there's an error - update the state object to display an error message
        .catch(error => {
          setError(error);
          setHierarchyLoaded(false);
        }
      )
  }

  useEffect(() => {
     getHierarchy()
      
  },[])

  return (
    <ThemeProvider theme = {theme}>
      <Router>
      <div className={classes.root}>
      {/* Use HeaderBar component, which contains the title and expandable filter panel */}
      <HeaderBar open = {open} handleDrawerClose = {handleDrawerClose} handleDrawerOpen = {handleDrawerOpen} theme = {theme} compactMode = {compactMode} setCompactMode = {setCompactMode}/>
      <FilterDrawer  open = {open} hierarchy = {hierarchy} hierarchyLoaded = {hierarchyLoaded} handleDrawerClose = {handleDrawerClose} handleDrawerOpen = {handleDrawerOpen} theme = {theme} getEmployees = {getEmployees} error = {error} resultCount = {resultCount} compactMode = {compactMode}></FilterDrawer>
      <main
        // Adapt the class name depending on if the filter panel is open or not
        className={clsx(classes.content, {
          [classes.contentShift]: open,
        })}
        >
        {displayLoadingIcon()}
        <div className={classes.drawerHeader} />

        {handleCompactMode()}

      </main>
    </div>
      </Router>
    </ThemeProvider>
  );
}

